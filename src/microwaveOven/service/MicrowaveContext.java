package microwaveOven.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import microwaveOven.util.Results;

public class MicrowaveContext {

	Results res = new Results();
	private MicrowaveStateI state;
	private int SetClockStateId = 0;
	private int checkingCounterOfSetStart = 0;
	private int numberFromInteger = 0;
	private int StopState = 0;
	private int testCaseNumber = 0;

	public void numbers(int num) {
		numberFromInteger += num;
	}

	public MicrowaveContext() {
		state = null;
	}

	public MicrowaveStateI getState() {
		return state;
	}

	public void numberMethod(String s) {
		res.generalmethod(s);
	}

	public void setState(MicrowaveStateI state) throws InterruptedException {

		DateTimeFormatter dtf;
		LocalDateTime now;

		if ((state.getClass().getSimpleName().equals("SetClockStateImpl")) && ((StopState == 0) || (StopState == 2))) {
			SetClockStateId = 1;
			this.state = state;
			res.writeToStdout("\nButton SetClock Pressed");
			res.generalmethod("\nButton SetClock Pressed");

		}

		if ((state.getClass().getSimpleName().equals("TestCase"))) {
			res.writeToStdout("______________________________________________");
			res.writeToStdout("\nTest Case number :" + testCaseNumber);
			res.writeToStdout("______________________________________________");
			res.generalmethod("______________________________________________");
			res.generalmethod("\nTest Case number :" + testCaseNumber);
			res.generalmethod("______________________________________________");

			testCaseNumber++;

		}

		if ((state.getClass().getSimpleName().equals("NumbersStateImpl")) && (SetClockStateId == 0)) {
			res.writeToStdout("\nInvalid Button Number Pressed");
			res.generalmethod("\nInvalid Button Number Pressed");
		}

		if ((state.getClass().getSimpleName().equals("SetClockStateImpl")) && ((StopState == 1))) {
			this.state = state;
			res.generalmethod("\nInvalid SetClock Pressed");
			res.writeToStdout("\nInvalid SetClock Pressed");
		}

		if ((state.getClass().getSimpleName().equals("SetStartStateImpl")) && (SetClockStateId == 1)
				&& (numberFromInteger >= 3)) {
			checkingCounterOfSetStart = 1;
			this.state = state;
			res.generalmethod("\nButton START Pressed");
			res.writeToStdout("\nButton START Pressed");
		}
		if ((state.getClass().getSimpleName().equals("SetStartStateImpl")) && (SetClockStateId == 0)) {
			res.writeToStdout("\nInvalid Button START Pressed");
			res.generalmethod("\nInvalid Button START Pressed");
		}

		if ((state.getClass().getSimpleName().equals("TestCase"))) {
			SetClockStateId = 0;
			checkingCounterOfSetStart = 0;
			numberFromInteger = 0;
			StopState = 0;
		}

		if ((state.getClass().getSimpleName().equals("StopStateImpl")) && (SetClockStateId == 1)) {
			now = LocalDateTime.now();
			dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			now = LocalDateTime.now();
			res.generalmethod(dtf.format(now));
			res.writeToStdout(dtf.format(now));
			// System.out.println(dtf.format(now));
			res.generalmethod("Button STOP Pressed");
			res.writeToStdout("Button STOP Pressed");
			StopState += 1;
		}

		if ((state.getClass().getSimpleName().equals("StopStateImpl")) && (SetClockStateId == 1)
				&& (checkingCounterOfSetStart == 0)) {
			SetClockStateId = 0;
			numberFromInteger = 0;
			res.generalmethod("\nInvalid Button Start Pressed");
			res.writeToStdout("\nInvalid Button Start Pressed");
			StopState += 1;
		}

	}

	public void printing() {
		res.writeToFile();
	}

}
