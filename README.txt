Abhishek Gupta
B00645446

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.

[Date: 24th June 2017 ]

To Unzip tar write
tar -xvzf abhishek_gupta_assign_2.tar.gz

To create a tar
tar -czvf abhishek_gupta_assign_2.tar.gz abhishek_gupta_assign_2/

## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile src/build.xml all

-----------------------------------------------------------------------
## To run by specifying arguments from command line 
ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt

Time Complexity : O(n)

The code checks for both input and output file and return error message if any is missing.