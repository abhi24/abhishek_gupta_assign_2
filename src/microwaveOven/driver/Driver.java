package microwaveOven.driver;

import java.util.ArrayList;
import java.util.List;
import microwaveOven.service.MicrowaveContext;
import microwaveOven.service.NumbersStateImpl;
import microwaveOven.service.SetClockStateImpl;
import microwaveOven.service.SetStartStateImpl;
import microwaveOven.service.StopStateImpl;
import microwaveOven.service.TestCase;
import microwaveOven.util.FileProcessor;
import microwaveOven.util.Results;

public class Driver {

	static Results res = new Results();
	static int counter = 0;
	static MicrowaveContext context = new MicrowaveContext();
	static int setClock = 0;
	static int counterTestCase = 0;
	String str;
	private static int isStartActive = 0;
	private static int isSetClockActive = 0;
	private static int isStopActive = 0;

	public static void main(String[] args) {
		List<String> arrList = new ArrayList<String>();
		int keycode = 0;

		String inputFile = null;
		String ouputFile = null;
		try {
			inputFile = args[0];
			if (!(inputFile.equals("input.txt"))) {
				System.out.println("No Input File Present Please Enter It");
				System.exit(0);
			}
		} catch (Exception e) {
			System.out.println("No Input File Present Please Enter It");
			System.exit(0);
		}

		try {
			ouputFile = args[1];
			if (!(ouputFile.equals("output.txt"))) {
				System.out.println("No Output File Present Please Enter It");
				System.exit(0);
			}
			res.outputFileName(ouputFile);
		} catch (Exception e) {
			System.out.println("No Output File Present Please Enter It");
			System.exit(0);
		}
		String str = null;
		FileProcessor fp = new FileProcessor();
		while ((str = fp.ReadLine(inputFile)) != null) {
			arrList.add(str);
		}

		for (int i = 0; i < arrList.size() + 1; i++) {

			if (i == arrList.size()) {
				context.printing();
				break;
			}

			if ((arrList.get(i).equals("TestCase")) || (arrList.get(i).equals("Reset"))) {
				counterTestCase++;
				keycode = 13;
				action(13);

			}
			if (arrList.get(i).equals("SetClockStateImpl")) {
				isSetClockActive = 1;
				keycode = 14;
				action(14);
			}
			if (arrList.get(i).equals("StopStateImpl")) {
				keycode = 11;
				isStopActive += 1;
				action(11);
			}
			if (arrList.get(i).equals("SetStartStateImpl")) {
				keycode = 12;
				// isStopActive = 0;
				action(12);
			}

			if (arrList.get(i).equals("0")) {
				keycode = 0;
				action(0);
				counter++;
			}
			if (arrList.get(i).equals("1")) {
				keycode = 1;
				action(1);
				counter++;
			}
			if (arrList.get(i).equals("2")) {
				keycode = 2;
				action(2);
				counter++;
			}

			if (arrList.get(i).equals("3")) {
				keycode = 3;
				action(3);
				counter++;
			}

			if (arrList.get(i).equals("4")) {
				keycode = 4;
				action(4);
				counter++;
			}

			if (arrList.get(i).equals("5")) {
				keycode = 5;
				action(5);
				counter++;
			}

			if (arrList.get(i).equals("6")) {
				keycode = 6;
				action(6);
				counter++;
			}

			if (arrList.get(i).equals("7")) {
				keycode = 7;
				action(7);
				counter++;
			}

			if (arrList.get(i).equals("8")) {
				keycode = 8;
				action(8);
				counter++;
			}

			if (arrList.get(i).equals("9")) {
				keycode = 9;
				action(9);
				counter++;
			}

		}

		// res.writeToFile();
	}

	private static void action(int keycode) {
		NumbersStateImpl num;
		switch (keycode) {
		case 11:
			StopStateImpl stop = new StopStateImpl();
			stop.doAction(context);
			break;

		case 12:

			if (counter >= 3 && isStopActive == 3) {
				context.numberMethod("Invalid Start Button Pressed");

				System.out.println("Invalid Start Button Pressed");
				counter = 0;
				break;
			} else if (counter >= 3) {
				SetStartStateImpl start = new SetStartStateImpl();
				start.doAction(context);
				isStartActive = 1;
				// counter = 0;
				break;
			} else {
				context.numberMethod("Invalid Start Button Pressed");
				System.out.println("Invalid Start Button Pressed");
				break;
			}

		case 13:
			TestCase reset = new TestCase();
			reset.doAction(context);
			break;

		case 14:
			SetClockStateImpl setclock = new SetClockStateImpl();
			setclock.doAction(context);
			break;

		case 1:
			num = new NumbersStateImpl();
			context.numberMethod("1");
			res.writeToStdout("1");
			context.numbers(1);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);

			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 1 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 1 Pressed");
				break;
			}
			break;

		case 2:
			num = new NumbersStateImpl();
			context.numberMethod("2");
			res.writeToStdout("2");
			context.numbers(2);
			if (counter >= 3 && isStartActive == 0)
				num.doAction(context);
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 2 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 2 Pressed");
				break;
			}

		case 3:
			num = new NumbersStateImpl();
			context.numberMethod("3");
			res.writeToStdout("3");
			context.numbers(3);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 3 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 3 Pressed");
				break;
			}
			break;
		case 4:
			num = new NumbersStateImpl();
			context.numberMethod("4");
			res.writeToStdout("4");
			context.numbers(4);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 4 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 4 Pressed");
				break;
			}
			break;
		case 5:
			num = new NumbersStateImpl();
			context.numberMethod("5");
			res.writeToStdout("5");
			context.numbers(5);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 5 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 5 Pressed");
				break;
			}
			break;

		case 6:
			num = new NumbersStateImpl();
			context.numberMethod("6");
			res.writeToStdout("6");
			context.numbers(6);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 6 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 6 Pressed");
				break;
			}
			break;
		case 7:
			num = new NumbersStateImpl();
			context.numberMethod("7");
			res.writeToStdout("7");
			context.numbers(7);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 7 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 7 Pressed");
				break;
			}
			break;
		case 8:
			num = new NumbersStateImpl();
			context.numberMethod("8");
			res.writeToStdout("8");
			context.numbers(8);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 8 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 8 Pressed");
				break;
			}
			break;
		case 9:
			num = new NumbersStateImpl();
			context.numberMethod("9");
			res.writeToStdout("9");
			context.numbers(9);
			if (counter >= 3 && isStartActive == 0) {
				num.doAction(context);
				break;
			}
			if (counter >= 3 && isSetClockActive == 1) {
				num.doAction(context);
			}
			if (counter >= 3 && isStartActive == 1) {

				String str = "Invalid NUMBER 9 Pressed";
				context.numberMethod(str);
				System.out.println("Invalid NUMBER 9 Pressed");
				break;
			}
			break;

		}

	}
}